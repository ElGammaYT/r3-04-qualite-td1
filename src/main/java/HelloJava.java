/**
 * Ceci est une classe de test qui ne fait pas grand chose
 * @author JMB
 * @author LC
 */
public class HelloJava {
	/**
	 * Methode qui retourne une phrase par defaut
	 * 
	 * @return Couac
	 */
	public void afficherCancan(){
		System.out.println("Couac");
	}

	/**
	 * Methode qui retourne une phrase par defaut
	 * 
	 * @param args
	 * @return Hello Blagnac
	 */
	public static void main(String[] args) {
		System.out.println("Hello Blagnac");
		TestApp app = new TestApp();
		System.out.println(app.hello());
	}
}
